/****************************************************************************
 *                                                                          *
 *    NAMIHEY  V2.01    2000/03/22                                          *
 *                                                                          *
 *    Copyright(C)  1999, 2000   TeamKNOx                                   *
 ****************************************************************************/

#include <gb.h>
#include <stdlib.h>

#define GUNPEY_MODE   0     /* 0:Namihey, 1:Gunpey */

extern UWORD max_score;     /* hi-score in backup ram */


/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define MODE_EXIT     0
#define MODE_OVER     1
#define MODE_MENU     2
#define MODE_ABOUT    3
#define MODE_ENDLESS  4
#define MODE_STAGE    5

/* panel condition */
#define PANEL_0       0x00  /* non line */
#define PANEL_1       0x01  /* down left, up right */
#define PANEL_2       0x02  /* up       , down     */
#define PANEL_3       0x04  /* up       , up       */
#define PANEL_4       0x08  /* down     , down     */
#define PANEL_FLASH   0x10  /* flash(rainbow) bit */
#define PANEL_BLINK   0x80  /* blink bit */

/* flag condition */
#define NONCHECK      0
#define SKIP          1
#define CONNECT       2

/* timer value */
#if GUNPEY_MODE /* hard mode */
#define SHIFTUP_TIME  350UL  /* first speed */
#define MAX_SHIFTUP   120UL  /* max speed */
#define FLASH_TIME    300UL  /* fixed */
#define BLINK_TIME    120UL  /* fixed */
#define RAINBOW_TIME  2UL    /* fixed */
#define BLINK_CYCLE   0x08UL /* fixed */
#define CHECK_DELAY   4UL    /* connect_chk() delay */
#else           /* easy mode (namihey) */
#define SHIFTUP_TIME  500UL  /* first speed */
#define MAX_SHIFTUP   150UL  /* max speed */
#define FLASH_TIME    350UL  /* fixed */
#define BLINK_TIME    180UL  /* fixed */
#define RAINBOW_TIME  4UL    /* fixed */
#define BLINK_CYCLE   0x10UL /* fixed */
#define CHECK_DELAY   6UL    /* connect_chk() delay */
#endif

/* for bgm engine */
#define	OFF           0
#define	ON            1
#define	BGM_TEMPO     6


/*==========================================================================*
 |  work area                                                               |
 *==========================================================================*/
UBYTE cursor_x;
UBYTE cursor_y;
UBYTE keycode_flag;
UBYTE keycode_off;
UBYTE stage;
UWORD panel_total;
UWORD panel_rem;
UWORD score_total;
UWORD s_timer, b_timer, f_timer, r_timer;
UWORD s_level;
UWORD bkg_rainbow[4];
UWORD rainbow;
unsigned char panel[7][12];
unsigned char panel2[7][12];
UBYTE flag[7][12];
UBYTE bgm;


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "obj.h"
#include "obj.c"
#include "bkg.c"
#include "ank_char.c"

/* panel design */
#if GUNPEY_MODE
#include "p151.h"
#include "p151.c" /* gunpey style */
#else
#include "p15.h"
#include "p15.c"  /* namihey style */
#endif

/* bgm code & data */
#include "sndplayer202f.c"
#include "Stardust.c"

/* palette data */
UWORD bkg_palette[] = {
    p15CGBPal0c0, p15CGBPal0c1, p15CGBPal0c2, p15CGBPal0c3,
    p15CGBPal1c0, p15CGBPal1c1, p15CGBPal1c2, p15CGBPal1c3,
    p15CGBPal6c0, p15CGBPal6c1, p15CGBPal6c2, p15CGBPal6c3,
    p15CGBPal7c0, p15CGBPal7c1, p15CGBPal7c2, p15CGBPal7c3
};
UWORD rainbow_palette[] = {
    0x001F, 0x00FF, 0x01FF, 0x02FF, /* red     -> yellow-  */
    0x03FF, 0x03F7, 0x03EF, 0x03E7, /* yellow  -> green-   */
    0x03E0, 0x1FE0, 0x3FE0, 0x5FE0, /* green   -> cyan-    */
    0x7FE0, 0x7EE0, 0x7DE0, 0x7CE0, /* cyan    -> blue-    */
    0x7C00, 0x7C07, 0x7C0F, 0x7C17, /* blue    -> magenta- */
    0x7C1F, 0x5C1F, 0x3C1F, 0x1C1F  /* magenta -> red-     */
};
UWORD obj_palette[] = {
    objCGBPal0c0, objCGBPal0c1, objCGBPal0c2, objCGBPal0c3,
    objCGBPal1c0, objCGBPal1c1, objCGBPal1c2, objCGBPal1c3
};

/* demo data */
unsigned char demo_panel[6][9] = {
    { 1, 2, 0, 8, 1, 0, 0, 0, 0 }, /* G */
    { 0, 0, 2, 0, 2, 1, 0, 0, 0 }, /* U */
    { 0, 1, 2, 0, 2, 0, 0, 0, 0 }, /* N */
    { 1, 2, 0, 2, 1, 2, 0, 0, 0 }, /* P */
    { 1, 2, 0, 0, 1, 4, 0, 0, 0 }, /* E */
    { 0, 2, 4, 0, 0, 2, 0, 0, 0 }  /* Y */
};
UBYTE demo_x[6] = { 1, 2, 4, 1, 3, 4 };
UBYTE demo_y[6] = { 5, 2, 1, 8, 7, 4 };

/* wall pattern */
#if GUNPEY_MODE
unsigned char gpy_head[]  = {
    0x8D,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8F,
    0x81,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x85,
};
#else
unsigned char gpy_head[]  = {
    0x8D,0x8E,0x8F,0x8D,0x8E,0x8F,0x8D,0x8E,0x8F,0x8D,0x8E,0x8F,
    0x81,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x85,
};
#endif
unsigned char gpy_line[] = {
    0x86,0x96,0x99,0x96,0x99,0x96,0x99,0x96,0x99,0x96,0x99,0x87,
    0x86,0x9C,0xA0,0x9C,0xA0,0x9C,0xA0,0x9C,0xA0,0x9C,0xA0,0x87,
    0x86,0x90,0x93,0x90,0x93,0x90,0x93,0x90,0x93,0x90,0x93,0x87
};
unsigned char gpy_tail[] = {
    0x88,0x89,0x8A,0x8B,0x89,0x8A,0x8B,0x89,0x8A,0x8B,0x89,0x8C
};
unsigned char gpy_head_a[] = { 7,7,7,7,7,7,7,7,7,7,7,7 };
unsigned char gpy_line_a[] = { 7,1,1,1,1,1,1,1,1,1,1,7 };

/* messages */
unsigned char gmsg_score[] = { "SCORE" };
unsigned char gmsg_panel[] = { "PANEL" };
unsigned char gmsg_stage[] = { "STAGE" };
unsigned char gmsg_top[]   = { "TOP" };
unsigned char gmsg_clr[]   = { "                    " };
unsigned char gmsg_clr_a[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char gmsg_0[]     = { "0" };
unsigned char gmsg_1[]     = { "1" };
unsigned char gmsg_zzz[]   = { "!" };  /* for debug */
unsigned char gmsg_over[]  = {  0, 1, 2, 3, 4, 5, 3, 6 };
unsigned char gmsg_bonus[] = {  7, 8, 3, 1, 6, 9,10,10,10 };
unsigned char gmsg_pause[] = { 11, 1,12,13, 3 };
unsigned char gmsg_stclr[] = { 13,14, 1, 0, 3,15, 7, 8, 6 };
unsigned char msg_about1[] = { "- ABOUT - " };
unsigned char msg_about2[] = { " K.Izumi  " };
unsigned char msg_about3[] = { " N.Ueda   " };
unsigned char msg_about4[] = { " O.Ohashi " };
unsigned char msg_about5[] = { " Y.Sato   " };
unsigned char msg_about6[] = { " G.Raimond" };
unsigned char msg_knox[] = { "`2000  by TeamKNOx" };
unsigned char msg_mode[][8] = { "- MENU -","NORMAL  ","STAGE   ","ABOUT   " };

/* tables for route checking */
BYTE  route1_x[4] = { -1,  0,  0,  1 }; 
BYTE  route1_y[4] = {  0, -1,  1,  0 };
UBYTE route1_p[4] = {  1,  3,  0,  2 };
BYTE  route2_x[4] = { -1,  1, -1,  1 }; 
BYTE  route2_y[4] = { -1, -1,  1,  1 };
UBYTE route2_p[4] = {  3,  2,  1,  0 };
BYTE  route3_x[4] = {  0,  1, -1,  0 }; 
BYTE  route3_y[4] = { -1,  0,  0,  1 };
UBYTE route3_p[4] = {  2,  0,  3,  1 };
UBYTE new_p[4][5] = {
   { 0, 0, 4, 2, 0 }, /* p=0 */
   { 0, 3, 0, 1, 0 }, /* p=1 */
   { 0, 2, 0, 0, 4 }, /* p=2 */
   { 0, 0, 1, 0, 3 }  /* p=3 */
};

/* tables for x1.5 display driver */
int pan_tbl[32] = { 0, 1, 2, 0, 3, 0, 0, 0, 4, 0,0,0,0,0,0,0,
                    0, 5, 6, 0, 7, 0, 0, 0, 8, 0,0,0,0,0,0,0 };
unsigned char lower_left[9]  = {
    0x90,0x91,0x92,0x92,0x91,0xA4,0xA5,0xA5,0xA4 };
unsigned char lower_right[9] = {
    0x93,0x94,0x95,0x94,0x95,0xA6,0xA7,0xA6,0xA7 };
unsigned char upper_left[9]  = {
    0x96,0x97,0x98,0x98,0x97,0xA8,0xA9,0xA9,0xA8 };
unsigned char upper_right[9] = {
    0x99,0x9A,0x9B,0x9A,0x9B,0xAA,0xAB,0xAA,0xAB };
unsigned char midle_left[81]  = {
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 0 */
    0x9D,0x9D,0x9F,0x9F,0x9D,0x9D,0xB2,0xB2,0x9D,   /* 1 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 2 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 3 */
    0x9D,0x9D,0x9F,0x9F,0x9D,0x9D,0xB2,0xB2,0x9D,   /* 4 */
    0xAC,0xAC,0xB3,0xB3,0xAC,0xAC,0xAE,0xAE,0xAC,   /* 5 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 6 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 7 */
    0xAC,0xAC,0xB3,0xB3,0xAC,0xAC,0xAE,0xAE,0xAC }; /* 8 */
unsigned char midle_right[81] = {
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 0 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 1 */
    0xA1,0xA3,0xA1,0xA3,0xA1,0xB4,0xA1,0xB4,0xA1,   /* 2 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 3 */
    0xA1,0xA3,0xA1,0xA3,0xA1,0xB4,0xA1,0xB4,0xA1,   /* 4 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 5 */
    0xAF,0xB5,0xAF,0xB5,0xAF,0xB1,0xAF,0xB1,0xAF,   /* 6 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 7 */
    0xAF,0xB5,0xAF,0xB5,0xAF,0xB1,0xAF,0xB1,0xAF }; /* 8 */


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
/*--------------------------------------------------------------------------*
 |  set attribute                                                           |
 *--------------------------------------------------------------------------*/
void set_bkg_attribute( UBYTE x,UBYTE y,UBYTE sx,UBYTE sy,unsigned char *p )
{
  if( _cpu == CGB_TYPE ) {
    VBK_REG = 1;
    set_bkg_tiles( x, y, sx, sy, p );
    VBK_REG = 0;
  }
}

void int_bgm()
{
  clock--;
  if( (!clock) && bgm ) {
    wait_vbl_done();
    music();
    clock = BGM_TEMPO;
  }
}

/*--------------------------------------------------------------------------*
 |  x1.5 panel display drivers                                              |
 *--------------------------------------------------------------------------*/
void x15driver( int x, int y )
{
  int xx, yy, pan;
  unsigned char tiles[4];

  xx = x * 2 - 1;
  yy = ((y-1)*3)/2+2;
  pan = pan_tbl[panel[x][y]&0x1F];

  if( y % 2 ) {
    tiles[0] = upper_left[pan];
    tiles[1] = upper_right[pan];
    pan = pan * 9 + pan_tbl[panel[x][y+1]&0x1F];
    tiles[2] = midle_left[pan];
    tiles[3] = midle_right[pan];
    set_bkg_tiles( xx, yy, 2, 2, tiles );
  } else {
    tiles[2] = lower_left[pan];
    tiles[3] = lower_right[pan];
    pan = pan + pan_tbl[panel[x][y-1]&0x1F] * 9;
    tiles[0] = midle_left[pan];
    tiles[1] = midle_right[pan];
    set_bkg_tiles( xx, yy, 2, 2, tiles );
  }
}

int obj_x15( int x, int y, int n )
{
  int xx, yy, pan;
  unsigned char tiles[4];

  if( n > 38L )  return( 40L ); /* overflow */
  xx = (x*2-1)*8 + 7;
  yy = (((y-1)*3)/2+2)*8 + 15;
  pan = pan_tbl[panel2[x][y]&0x1F];

  if( y % 2 ) {
    tiles[0] = upper_left[pan];
    tiles[1] = upper_right[pan];
    set_sprite_tile( n  , tiles[0]-0x80 );
    set_sprite_tile( n+1, tiles[1]-0x80 );
    move_sprite( n  , xx,   yy );
    move_sprite( n+1, xx+8, yy );
    n += 2L;
    if( n > 38L )  return( 40L );
    if( panel2[x][y+1] == 0 ) {
      pan = pan * 9;
      tiles[2] = midle_left[pan];
      tiles[3] = midle_right[pan];
      set_sprite_tile( n  , tiles[2]-0x80 );
      set_sprite_tile( n+1, tiles[3]-0x80 );
      move_sprite( n  , xx,   yy+8 );
      move_sprite( n+1, xx+8, yy+8 );
      n += 2L;
    }
  } else {
    tiles[2] = lower_left[pan];
    tiles[3] = lower_right[pan];
    pan = pan + pan_tbl[panel2[x][y-1]&0x1F] * 9;
    tiles[0] = midle_left[pan];
    tiles[1] = midle_right[pan];
    set_sprite_tile( n  , tiles[0]-0x80 );
    set_sprite_tile( n+1, tiles[1]-0x80 );
    move_sprite( n  , xx,   yy );
    move_sprite( n+1, xx+8, yy );
    if( n > 36L )  return( 40L );
    set_sprite_tile( n+2, tiles[2]-0x80 );
    set_sprite_tile( n+3, tiles[3]-0x80 );
    move_sprite( n+2, xx,   yy+8 );
    move_sprite( n+3, xx+8, yy+8 );
    n += 4L;
  }
  return( n );
}

void cls_obj( int n )
{
  int i;

  for( i=n; i<40; i++ ) {
    move_sprite( i, 0, 0 );
  }
}

/*--------------------------------------------------------------------------*
 |  display value (dword)                                                   |
 *--------------------------------------------------------------------------*/
void disp_value( UBYTE x, UBYTE y, UWORD d )
{
  UBYTE i, f;
  unsigned char data[6];

  for( i=0; i<6; i++ ) {
    data[5-i] = d % 10;
    d = d / 10;
  }
  f = 0;
  for( i=0; i<6; i++ ) {
    if( f || data[i] ) {
      f = 1;
      data[i] += '0';
    } else {
      data[i] = ' ';
    }
  }
  set_bkg_tiles( x, y, 6, 1, data );
}

void cls_menu()
{
  UBYTE i;

  for( i=0; i<18; i++ ) {
    set_bkg_tiles( 12, i, 8, 1, gmsg_clr );
  }
}

/*--------------------------------------------------------------------------*
 |  panel controls                                                          |
 *--------------------------------------------------------------------------*/
void disp_panel()
{
  UBYTE x, y;
  unsigned char tiles[2];

  for( x=1; x<6; x++ ) {
    tiles[0] = tiles[1] = x;
    for( y=2; y<17; y++ ) {
      set_bkg_attribute( 11-x*2, y, 2, 1, tiles );
    }
    for( y=1; y<11; y++ ) {
      x15driver( 6-x, y );
    }
  }
}

void disp_panel2()
{
  UBYTE x, y;
  int   n;

  n = 6L;
  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      if ( panel2[x][y] & 0x1F ) {
        n = obj_x15( x, y, n );
      }
    }
  }
  cls_obj( n );
}

void clear_panel()
{
  UBYTE x, y;

  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      panel[x][y] = panel2[x][y] = 0;
    }
  }
  disp_panel();
  cls_obj( 6 );
}

/*--------------------------------------------------------------------------*
 |  player(box) controls                                                    |
 *--------------------------------------------------------------------------*/
void hide_box()
{
  UBYTE i;

  for( i=0; i<6; i++ ) {
    move_sprite( i, 0, 0 );
  }
}

void show_box()
{
  UBYTE x, y;

  x = 16 * cursor_x;
  y = 12 * cursor_y + 20;
  move_sprite( 0, x,   y    );
  move_sprite( 1, x,   y+8  );
  move_sprite( 2, x,   y+16 );
  move_sprite( 3, x+8, y    );
  move_sprite( 4, x+8, y+8  );
  move_sprite( 5, x+8, y+16 );
}

/*--------------------------------------------------------------------------*
 |  pad driver                                                              |
 *--------------------------------------------------------------------------*/
UBYTE move_box( UBYTE mode )
{
  UBYTE i, key;

  key = joypad();
  if( keycode_flag == 0 ) {
    if( key & (J_UP|J_DOWN|J_LEFT|J_RIGHT|J_A|J_B|J_START) ) {
      keycode_flag = 1;
      if( key & J_UP ) {
        if(cursor_y > 1 )           cursor_y--;
      } else if( key & J_DOWN ) {
        if( cursor_y < 9 )          cursor_y++;
      } else if( key & J_LEFT ) {
        if( cursor_x > 1 )          cursor_x--;
      } else if( key & J_RIGHT ) {
        if( cursor_x < 5 )          cursor_x++;
      } else if( key & J_A ) {
        keycode_off |= J_A;
      } else if( key & J_B ) {
        keycode_off |= J_B;
      } else if( key & J_START ) {
        keycode_off |= J_START;
      }
      show_box();
    }
  } else if( !(key & (J_UP|J_DOWN|J_LEFT|J_RIGHT|J_A|J_B|J_START)) ) {
    keycode_flag = 0;
  }
  return( mode );
}

void move_cursor()
{
  UBYTE key, x, y;

  key = joypad();
  if( keycode_flag == 0 ) {
    if( key & (J_UP|J_DOWN|J_A) ) {
      keycode_flag = 1;
    }
    if( key & J_UP ) {
      if( cursor_y > 0 )            cursor_y--;
    } else if( key & J_DOWN ) {
      if( cursor_y < 2 )            cursor_y++;
    } else if( key & J_A ) {
      keycode_off |= J_A;
    }
  } else if( !(key & (J_UP|J_DOWN|J_A)) ) {
    keycode_flag = 0;
  }

  x = 104;
  y = cursor_y * 16 + 40;
  move_sprite( 0, x,    y );
  move_sprite( 1, x+56, y );
}

/*--------------------------------------------------------------------------*
 |  route check (recursive engine)                                          |
 *--------------------------------------------------------------------------*/
void check_panel( UBYTE x, UBYTE y, UBYTE p, UBYTE *connect_flags )
{
  UBYTE s;

  if( (x<1) || (x>5) || (y<1) || (y>10) )  return;

  if( p=new_p[p][pan_tbl[panel[x][y]&0x0F]] ) {
    p--;
    s = *connect_flags; /* save check counter */
    if( (x==5) && (p&0x01) ) {
      *(connect_flags+1) = CONNECT;
      flag[x][y] = s;
      *connect_flags += 1;
    } else if( (x==1) && !(p&0x01) && *(connect_flags+1) ) {
      flag[x][y] = s;
      *connect_flags += 1;
    } else if( flag[x][y] == NONCHECK ) {
      flag[x][y] = s;
    } else if( flag[x][y] == SKIP ) {
      return;
    } else if( flag[x][y] < s ) {
      *connect_flags += 1;
      return;
    } else {
      return;
    }
    check_panel( x+route1_x[p], y+route1_y[p], route1_p[p], connect_flags );
    check_panel( x+route2_x[p], y+route2_y[p], route2_p[p], connect_flags );
    check_panel( x+route3_x[p], y+route3_y[p], route3_p[p], connect_flags );
    if( s == *connect_flags ) {
      flag[x][y] = SKIP;
    }
#if 1 /* for debug */
    if( s > 50 ) {
      set_bkg_tiles( 19, 16, 1, 1, gmsg_zzz );      
      disp_value( 13, 17, (int)s );
      waitpad( J_START );
    }
#endif
  }
}

/*--------------------------------------------------------------------------*
 |  connection check                                                        |
 *--------------------------------------------------------------------------*/
UBYTE connect_chk( UBYTE mode )
{
  UBYTE x, y, connect_flags[2];

  /* clear flag table */
  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      flag[x][y] = NONCHECK;
    }
  }

  /* check the route */
  connect_flags[0] = CONNECT; /* init check counter */
  for( y=1; y<12; y++ ) {
    mode = move_box( mode );
    connect_flags[1] = NONCHECK; /* init connect flag */
    check_panel( 1, y-1, 2, connect_flags );
    check_panel( 1, y,   0, connect_flags );
  }

  /* judge */
  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      if( flag[x][y] >= CONNECT ) {
        if( !f_timer ) {  /* first time */
          f_timer = FLASH_TIME;
        }
        panel[x][y] |=  PANEL_FLASH;  /* rainbow */
        panel[x][y] &= ~PANEL_BLINK;  /* stop blinking */
        x15driver( x, y );
      }
    }
  }
  return( mode );
}

/*--------------------------------------------------------------------------*
 |  shiftup and panel generate                                              |
 *--------------------------------------------------------------------------*/
UBYTE shift2add( UBYTE mode )
{
  UBYTE x, y, r, n, retry;

  if( !s_timer ) {
    s_timer = s_level;
    for( x=1; x<6; x++ ) {
      if( panel[x][1] != 0 ) {
        mode = MODE_OVER;  /* game over */
      }
    }
    for( y=1; y<10; y++ ) {
      for ( x=1; x<6; x++ ) {
        panel[x][y] = panel[x][y+1] & ~PANEL_BLINK;
        x15driver( x, y );
      } 
    }
    if( cursor_y > 1 ) {
      cursor_y--;
      show_box();
    }

    /* panel generation engine */
    retry = 1;
    while( retry ) {
      for( x=1; x<6; x++ ) {
        r = rand() % 8;
        if( r < 4 ) {
          panel[x][10] = (0x01 << r) & 0x0F | PANEL_BLINK;
          retry = 0;
        } else {
          panel[x][10] = PANEL_0;
        }
        x15driver( x, 10 );
      }
    }
    b_timer = BLINK_TIME;
    mode = connect_chk( mode );
  } else {
    s_timer--;
    delay( CHECK_DELAY );
  }
  return( mode );
}

/*--------------------------------------------------------------------------*
 |  panel delete                                                            |
 *--------------------------------------------------------------------------*/
UBYTE flash2del( UBYTE mode )
{
  UBYTE i, x, y, bonus;
  int   new;
  unsigned char *p;

  /* delete panels */
  if( f_timer ) {
    if( s_timer ) {
      s_timer = MAX_SHIFTUP;
    }
    f_timer--;
    if( f_timer == 0 ) {
      new = bonus = 0;
      for( y=1; y<11; y++ ) {
        for( x=1; x<6; x++ ) {
          if( flag[x][y] >= CONNECT ) {
            new++;
            panel[x][y]  = panel2[x][y];
            panel2[x][y] = 0;
            x15driver( x, y );
          }
          bonus |= panel[x][y];
          bonus |= panel2[x][y];
        }
      }
      disp_panel2();
      if( new > 0 ) {
        if( !bonus ) {
          score_total += 100L;  /* clean bonus */
          /* clear 1000 */
          set_sprite_data( 16, 16, ank_char );
          for( i=0; i<9; i++ ) {
            set_sprite_prop( i+6, 2 );
            set_sprite_tile( i+6, gmsg_bonus[i]+16 );
            move_sprite( i+6, i*9+15, 88 );
          }
          for( i=0; i<200; i++ ) {
            mode = move_box( mode );
            delay( 0x10 );
          }
          for( i=0; i<9; i++ ) {
            move_sprite( i+6, 0, 0 );
            set_sprite_prop( i+6, 1 );
          }
          p = p15;
          p += 256; /* offset */
          set_sprite_data( 16, 16, p ); /* reload */
          clear_panel(); /* safty code */
          /* speed down */
          s_level = SHIFTUP_TIME;
        } else {
          /* speed up */
          s_level = panel_total * (rand()%3+2);
          if( s_level > (SHIFTUP_TIME - MAX_SHIFTUP) ) {
            s_level = MAX_SHIFTUP;
          } else {
            s_level = SHIFTUP_TIME - s_level;
          }
        }
        score_total += (new*((UWORD)new-4L));
        panel_total += new;
        disp_value( 12, 3, score_total );
        disp_value( 13, 6, panel_total );

        if( mode == MODE_STAGE ) {
          if( panel_rem <= new ) {
            clear_panel();
            set_sprite_data( 16, 16, ank_char );
            for( i=0; i<9; i++ ) {
              set_sprite_prop( i+6, 2 );
              set_sprite_tile( i+6, gmsg_stclr[i]+16 );
              move_sprite( i+6, i*9+15, 88 );
            }
            for( i=0; i<200; i++ ) {
              mode = move_box( mode );
              delay( 0x10 );
            }
            for( i=0; i<9; i++ ) {
              move_sprite( i+6, 0, 0 );
              set_sprite_prop( i+6, 1 );
            }
            p = p15;
            p += 256; /* offset */
            set_sprite_data( 16, 16, p ); /* reload */
            stage++;
            disp_value( 13, 9, stage );
            panel_rem = 12 + stage *3;
          } else {
            panel_rem -= new;
          }
        }
      }
      mode = connect_chk( mode );
    } else {
      delay( CHECK_DELAY );
    }
  } else {
    mode = shift2add( mode );
  }

  /* blink panels */
  if( b_timer ) {
    b_timer--;
    if( b_timer == 0 ) {
      for( x=1; x<6; x++ ) {
        panel[x][10] &= ~PANEL_BLINK;
        x15driver( x, 10 );
      }
    } else if( b_timer & BLINK_CYCLE ) {
      for( x=1; x<6; x++ ) {
        if( panel[x][10] & PANEL_BLINK ) {
          i = panel[x][10];
          panel[x][10] = 0;
          x15driver( x, 10 );
          panel[x][10] = i;
        }
      }
    } else {
      for( x=1; x<6; x++ ) {
        x15driver( x, 10 );
      }
    }
  }

  if( r_timer ) {
    r_timer--;
  } else {
    r_timer = RAINBOW_TIME;
    rainbow++; if( rainbow>23 ) rainbow=0;
    for( i=0; i<5; i++ ) {
      bkg_rainbow[2] = rainbow_palette[(rainbow+i)%24];
      set_bkg_palette( i+1, 1, bkg_rainbow );
    }
  }
  return( mode );
}

/*--------------------------------------------------------------------------*
 |  main loop of game                                                       |
 *--------------------------------------------------------------------------*/
UBYTE gunpey_game( UBYTE mode )
{
  UBYTE i, x, y;

  /* setup sprite screen */
  set_sprite_data( 0, 16, obj ); /* overwrite 0-9 */
  for( i=0; i<6; i++ ) {
    set_sprite_tile( i, 3+i );
    move_sprite( i, 0, 0 );
    set_sprite_prop( i, 0 );
  }
  set_sprite_palette( 1, 1, &obj_palette[4] );
  for( i=6; i<40; i++ ) {
    move_sprite( i, 0, 0 );
    set_sprite_prop( i, 1 );
  }
  cls_menu();
  set_bkg_tiles( 13,  2,  5, 1, gmsg_score );
  set_bkg_tiles( 18,  3,  1, 1, gmsg_0 );
  set_bkg_tiles( 13,  5,  5, 1, gmsg_panel );
  set_bkg_tiles( 18,  6,  1, 1, gmsg_0 );
  if( mode == MODE_STAGE ) {
    stage = 1;
    set_bkg_tiles( 13,  8,  5, 1, gmsg_stage );
    set_bkg_tiles( 18,  9,  1, 1, gmsg_1 );
  }
  ENABLE_RAM_MBC1;
  SWITCH_RAM_MBC1( 0 );
  set_bkg_tiles( 13, 13,  3, 1, gmsg_top );
  disp_value( 12, 14, max_score );
  set_bkg_tiles( 18, 14,  1, 1, gmsg_0 );
  cursor_x = 3;
  cursor_y = 6;
  show_box();
  clear_panel();

  f_timer = b_timer = r_timer = 0L;
  s_timer = MAX_SHIFTUP;
  s_level = SHIFTUP_TIME;
  score_total = panel_total = 0L;
  panel_rem = 15;

  step   = 0;
  patern = 0;
  bgm    = ON;
  clock  = BGM_TEMPO;

  while( mode >= MODE_ENDLESS ) {
    mode = flash2del( mode );
    mode = move_box( mode );
    if( keycode_off & J_A ) {
      keycode_off &= ~J_A;
      x = cursor_x;
      y = cursor_y;
      if( flag[x][y] < CONNECT ) {
        i = panel[x][y] & ~PANEL_FLASH;
        if( flag[x][y+1] < CONNECT ) {
          panel[x][y] = panel[x][y+1] & ~PANEL_FLASH;
          panel[x][y+1] = i;
          x15driver( x, y+1 );
        } else {
          panel[x][y] = panel2[x][y+1];
          panel2[x][y+1] = i;
          disp_panel2();
        }
        x15driver( x, y );
        i |= panel[x][y];
      } else {
        i = panel2[x][y];
        if( flag[x][y+1] < CONNECT ) {
          panel2[x][y] = panel[x][y+1] & ~PANEL_FLASH;
          panel[x][y+1] = i;
          x15driver( x, y+1 );
        } else {
          panel2[x][y] = panel2[x][y+1];
          panel2[x][y+1] = i;
        }
        disp_panel2();
        i |= panel2[x][y];
      }
      if( i ) {
        mode = connect_chk( mode );
      } else {
        delay( CHECK_DELAY );
      }
    } else if( keycode_off & J_B ) {
      keycode_off &= ~J_B;
      f_timer = 1;
      s_timer = 0;
    } else if( keycode_off & J_START ) {
      if( mode >= MODE_ENDLESS ) {
        keycode_off &= ~J_START;
        hide_box();
        stopmusic();
        bgm = OFF;
        delay( 0x40 );
        /* pause */
        set_sprite_data( 0, 16, ank_char );
        for( i=0; i<5; i++ ) {
          set_sprite_prop( i, 2 );
          set_sprite_tile( i, gmsg_pause[i] );
          move_sprite( i, i*11+30, 88 );
        }
        waitpadup();
        waitpad( J_START );
        waitpadup();
        set_sprite_data( 0, 16, obj ); /* overwrite 0-9 */
        for( i=0; i<6; i++ ) {
          set_sprite_tile( i, 3+i );
          move_sprite( i, 0, 0 );
          set_sprite_prop( i, 0 );
        }
        disp_panel();
        show_box();
        bgm = ON;
        clock = BGM_TEMPO;
      }
    } else {
      delay( CHECK_DELAY );
    }
  }
  return( mode );
}

/*--------------------------------------------------------------------------*
 |  demo                                                                    |
 *--------------------------------------------------------------------------*/
void gunpey_demo()
{
#if 0 /* 00/03/22: hidden demo strings */
  UBYTE i, j, x, y, n;

  for( i=0; i<6; i++ ) {
    for( j=5; j>=demo_x[i]; j-- ) {
      n = 0;
      for( x=0; x<3; x++ ) {
        if( x < (6-j) ) {
          for( y=0; y<3; y++ ) {
            panel[j+x][demo_y[i]+y] = demo_panel[i][n];
            x15driver( j+x, demo_y[i]+y );
            n++;
          }
        }
      }
      delay( 0x0020 );
    }
  }
#endif
}

/*--------------------------------------------------------------------------*
 |  menu                                                                    |
 *--------------------------------------------------------------------------*/
UBYTE gunpey_menu( UBYTE mode )
{
  UBYTE i;
  fixed seed;

  stopmusic();
  bgm = OFF;

  cls_obj( 0 );
  cls_menu();
  clear_panel();
  gunpey_demo();
  set_sprite_data( 0, 16, obj ); /* overwrite 0-9 */

  set_bkg_tiles( 12, 1, 8, 1, msg_mode[0] );
  for( i=0; i<3; i++ ){
    set_bkg_tiles( 13, i*2+3, 7, 1, msg_mode[i+1] );
  }

  set_sprite_tile( 0, 1 );
  set_sprite_tile( 1, 2 );

  keycode_flag = keycode_off = rainbow = 0;
  cursor_y = 0;
  while( !(keycode_off & J_A) ) {
    move_cursor();
  }
  switch( cursor_y ) {
    case 0:
      mode = MODE_ENDLESS;
      break;
    case 1:
      mode = MODE_STAGE;
      break;
    case 2:
      mode = MODE_ABOUT;
      break;
  }

  /* init rand */
  seed.b.l = DIV_REG;
  waitpadup();
  seed.b.h = DIV_REG;
  initarand( seed.w );

  return( mode );
}

/*--------------------------------------------------------------------------*
 |  over                                                                    |
 *--------------------------------------------------------------------------*/
UBYTE gunpey_over( UBYTE mode )
{
  UBYTE i;

  stopmusic();
  bgm = OFF;

  hide_box();

  /* game over */
  set_sprite_data( 0, 16, ank_char );
  for( i=0; i<8; i++ ) {
    set_sprite_prop( i, 2 );
    set_sprite_tile( i, gmsg_over[i] );
    move_sprite( i, i*9+20, 88 );
  }

  ENABLE_RAM_MBC1;
  SWITCH_RAM_MBC1( 0 );
  if( max_score < score_total ) {
    max_score = score_total;
  }
  disp_value( 12, 14, score_total );

  waitpadup();
  waitpad( J_START );
  waitpadup();

  return( MODE_MENU );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void gunpey_init()
{
  UBYTE i;

  score_total = 0L;

  /* load characters */
  set_bkg_data( 0, 128, bkg );
  set_bkg_data( 128, 64, p15 );
  set_sprite_data( 0, 64, p15 );

  /* setup palettes */
  set_bkg_palette( 0, 1, &bkg_palette[0] );
  bkg_rainbow[0] = bkg_palette[4];
  bkg_rainbow[1] = bkg_palette[5];
  bkg_rainbow[2] = bkg_palette[6];
  bkg_rainbow[3] = bkg_palette[7];
  for( i=1; i<=5; i++ ) {
    set_bkg_palette( i, 1, bkg_rainbow );
  }
  set_bkg_palette( 6, 1, &bkg_palette[8] );
  set_bkg_palette( 7, 1, &bkg_palette[12] );
  set_sprite_palette( 0, 1, &obj_palette[0] );
  set_sprite_palette( 1, 1, &obj_palette[4] );
  set_sprite_palette( 2, 1, &bkg_palette[8] );

  set_bkg_tiles( 0, 0, 12, 2, gpy_head );
  for( i=0; i<5; i++ ) {
    set_bkg_tiles( 0, i*3+2, 12, 3, gpy_line );
    set_bkg_attribute( 0, i*3+2, 12, 1, gpy_line_a );
    set_bkg_attribute( 0, i*3+3, 12, 1, gpy_line_a );
    set_bkg_attribute( 0, i*3+4, 12, 1, gpy_line_a );
  }
  set_bkg_tiles( 0, 17, 12, 1, gpy_tail );
  set_bkg_attribute( 0,  0, 12, 1, gpy_head_a );
  set_bkg_attribute( 0,  1, 12, 1, gpy_head_a );
  set_bkg_attribute( 0, 17, 12, 1, gpy_head_a );

  clear_panel();
}

/*--------------------------------------------------------------------------*
 |  about                                                                   |
 *--------------------------------------------------------------------------*/
UBYTE gunpey_about( UBYTE mode )
{
  UBYTE y;

  cls_obj( 0 );
  for( y=0; y<18; y++ ) {
    set_bkg_tiles( 0, y, 20, 1, gmsg_clr );
    set_bkg_attribute( 0, y, 20, 1, gmsg_clr_a );
  }

  set_bkg_tiles( 5,  2, 10, 1, msg_about1 );
  set_bkg_tiles( 4,  5, 10, 1, msg_about2 );
  set_bkg_tiles( 4,  7, 10, 1, msg_about3 );
  set_bkg_tiles( 4,  9, 10, 1, msg_about4 );
  set_bkg_tiles( 4, 11, 10, 1, msg_about5 );
  set_bkg_tiles( 4, 13, 10, 1, msg_about6 );
  set_bkg_tiles( 1, 16, 18, 1, msg_knox );

  waitpadup();
  waitpad( J_START );
  waitpadup();

  for( y=0; y<18; y++ ) {
    set_bkg_tiles( 0, y, 20, 1, gmsg_clr );
    set_bkg_attribute( 0, y, 20, 1, gmsg_clr_a );
  }
  gunpey_init();

  return( MODE_MENU );
}

/*--------------------------------------------------------------------------*
 |  main                                                                    |
 *--------------------------------------------------------------------------*/
void main()
{
  UBYTE mode;

  disable_interrupts();

  /* initialize bank number */
  SWITCH_ROM_MBC1( 0 );

  add_VBL( int_bgm );
  set_interrupts( VBL_IFLAG );
  stopmusic();
  bgm = OFF;
  patern_definition();

  gunpey_init();

  SHOW_BKG;
  SHOW_SPRITES;
  DISPLAY_ON;
  enable_interrupts();

  if( joypad() & J_START ) {
    ENABLE_RAM_MBC1;
    SWITCH_RAM_MBC1( 0 );
    max_score = 0L;
  }

  mode = MODE_MENU;

  while( 1 ) {

    switch( mode ) {

      case MODE_STAGE:
      case MODE_ENDLESS:
        mode = gunpey_game( mode );
        break;

      case MODE_ABOUT:
        mode = gunpey_about( mode );
        break;

      case MODE_OVER:
        mode = gunpey_over( mode );
        break;

      case MODE_MENU:
      default:
        mode = gunpey_menu( mode );
        break;
    }
  }
}

/* EOF */