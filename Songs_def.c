/*
	File for the music definitions to use with the Lemon Player

	created with mod2gbl 0.6

	Whoops, in fact you will need to create this file with the part of the song data
	created with mod2gbl.
	Just follow this exemple

	on 26 sept 99

	lemon@urbanet.ch

*/


int patern ;
int nbr_patern ;
int song_nbr ;
UWORD *data_song_ptr;

/*
extern UWORD data_song_lover0[];
extern UWORD data_song_lover1[];
extern UWORD data_song_lover2[];
extern UWORD data_song_lover3[];
extern UWORD data_song_lover4[];
extern UWORD data_song_lover5[];
extern UWORD data_song_lover6[];
extern UWORD data_song_lover7[];

void patern_definition()
{
    if(song_nbr == 0){
         if(patern == 0){ data_song_ptr = data_song_lover0 ; }
         if(patern == 1){ data_song_ptr = data_song_lover1 ; }
         if(patern == 2){ data_song_ptr = data_song_lover2 ; }
         if(patern == 3){ data_song_ptr = data_song_lover3 ; }
         if(patern == 4){ data_song_ptr = data_song_lover4 ; }
         if(patern == 5){ data_song_ptr = data_song_lover5 ; }
         if(patern == 6){ data_song_ptr = data_song_lover6 ; }
         if(patern == 7){ data_song_ptr = data_song_lover7 ; }
    }
}
*/

extern UWORD data_song_stardust0[];
extern UWORD data_song_stardust1[];
extern UWORD data_song_stardust2[];
extern UWORD data_song_stardust3[];
extern UWORD data_song_stardust4[];
extern UWORD data_song_stardust5[];
extern UWORD data_song_stardust6[];
extern UWORD data_song_stardust7[];

void patern_definition()
{
         if(patern == 0){ data_song_ptr = data_song_stardust0 ; }
         if(patern == 1){ data_song_ptr = data_song_stardust1 ; }
         if(patern == 2){ data_song_ptr = data_song_stardust2 ; }
         if(patern == 3){ data_song_ptr = data_song_stardust3 ; }
         if(patern == 4){ data_song_ptr = data_song_stardust4 ; }
         if(patern == 5){ data_song_ptr = data_song_stardust5 ; }
         if(patern == 6){ data_song_ptr = data_song_stardust6 ; }
         if(patern == 7){ data_song_ptr = data_song_stardust7 ; }
}


